import Board
import View

import Text.Read (readMaybe)

loop :: Board -> IO ()
loop b = do
    putStrLn ""
    printBoard b
    action <- getLine
    case (words action) of
        ("add":xs) -> do
            loop (snd $ addTodo (unwords xs) b)
        ("done":xs) -> do
            case readMaybe $ unwords xs :: Maybe Int of
                Just i -> loop $ toDone i b
                Nothing -> loop b
        ("quit":_) -> return ()
        _ -> loop b

main :: IO ()
main = loop newBoard